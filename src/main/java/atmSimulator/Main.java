package atmSimulator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/*
public class Main {

    static Logger logger = LoggerFactory.getLogger(Main.class);


    public static void main(String[] args) {
        Card crd = new Card(null,"2525252525252525", 1234, "Costel", "Vasile", 100.00, CardStatus.Available);
        Transactions trs = new Transactions();
        List<Transactions> transactionList = Arrays.asList(trs);
        crd.setTransactions(transactionList);

        try {
            DbOperations.insertNewTransactionByCard(crd,transactionList);
        } catch (Throwable e) {
            logger.error("Error in method main: ", e);
        } finally {
            HibernateUtils.cleanUp();
        }
    }
}

 */

public class Main extends Application {
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/atm.fxml"));
        Scene scene = new Scene(root);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        Image icon = new Image("/images/icon.png");
        primaryStage.getIcons().add(icon);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}