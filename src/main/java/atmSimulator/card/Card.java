package atmSimulator.card;

import homeBanking.account.Account;
import atmSimulator.transaction.Transactions;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "card")
@NamedQuery(name = "getCard", query = "from Card where ccn =:cardNumber")
@NamedQuery(name = "getAccountFromCard", query = "from Card where account_id=:accountIndex")
public class Card {

    @Id
    private String ccn;
    private String pin;
    private String firstName;
    private String lastName;
    private int amount;
    private CardStatus cardStatus;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "card")
    private List<Transactions> transactions;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;


    public Card() {
    }

    public Card(String ccn, String pin, String firstName, String lastName, int amount, CardStatus cardStatus, List<Transactions> transactions) {
        this.ccn = ccn;
        this.pin = pin;
        this.firstName = firstName;
        this.lastName = lastName;
        this.amount = amount;
        this.cardStatus = cardStatus;
        this.transactions = transactions;
    }

    public String getCcn() {
        return ccn;
    }

    public void setCcn(String ccn) {
        this.ccn = ccn;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public CardStatus getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(CardStatus cardStatus) {
        this.cardStatus = cardStatus;
    }

    public List<Transactions> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transactions> transactions) {
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        return ccn;
    }
}
