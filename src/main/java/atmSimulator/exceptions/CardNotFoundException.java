package atmSimulator.exceptions;

public class CardNotFoundException extends Exception {


    private String message;

    public CardNotFoundException(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public CardNotFoundException() {
    }
}


