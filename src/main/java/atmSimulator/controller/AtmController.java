package atmSimulator.controller;

import atmSimulator.card.Card;
import atmSimulator.card.CardStatus;
import atmSimulator.db.DbOperations;
import atmSimulator.exceptions.CardNotFoundException;
import atmSimulator.transaction.Transactions;
import com.jfoenix.controls.JFXButton;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AtmController implements Initializable {

    @FXML
    private TextField moneyTextField;
    @FXML
    private Button newAccountBtn;
    @FXML
    private Button otherAmountBtn;
    @FXML
    private TextField ccnTextField;
    @FXML
    private Button cardBtn;
    @FXML
    private Button ejectCardBtn;
    @FXML
    private TextArea infoArea;
    @FXML
    private Label labelDayHour;
    @FXML
    private Label labelHour;
    @FXML
    private Label labelCardNumber;
    @FXML
    private Label labelBalance;
    @FXML
    private Label labelWithdrawAmmount;
    @FXML
    private TextField pinField;
    @FXML
    private GridPane withdrawGridPane;
    @FXML
    private CheckBox checkBox1000;
    @FXML
    private CheckBox checkBox500;
    @FXML
    private CheckBox checkBox100;
    @FXML
    private CheckBox checkBox200;
    @FXML
    private CheckBox checkbox400;
    @FXML
    private JFXButton balanceBtn;
    @FXML
    private JFXButton withdrawalBtn;
    @FXML
    private JFXButton cancelTransactionBtn;
    @FXML
    private Button one, two, three, four, five, six, seven, eight, nine, zero, cancel, clear, enter;
    private String todaysdate, todaystime;
    private int attempt = 2;
    private Card card;
    private int money;
    private String checkBoxAmount;


    private ActionEvent event;

    // Display time and date
    private void displayTime() {
        DateTimeFormatter dateformat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter timeformat = DateTimeFormatter.ofPattern("hh:mm:ss a");

        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            LocalDateTime now = LocalDateTime.now();
            todaysdate = dateformat.format(now);
            todaystime = timeformat.format(now);
            labelDayHour.setText("Date : " + todaysdate);
            labelHour.setText("Time : " + todaystime);
        }),
                new KeyFrame(Duration.seconds(1))
        );
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    public void loadCard(String getCard) {
        try {
            card = DbOperations.getCard(getCard);
        } catch (CardNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void validatePin(String pin) {
        try {
            labelCardNumber.setFont(Font.font("Times New Roman", 20));

            if (attempt > 0) {
                if (!pin.equals(card.getPin())) {
                    attempt--;
                    labelCardNumber.setText("Pinul este gresit!");
                } else {
                    labelCardNumber.setText("Welcome account : " + card.getFirstName() + " " + card.getLastName() + "\n"
                            + " Account Number : " + card.getCcn());
                    balanceBtn.setDisable(false);
                    withdrawalBtn.setDisable(false);
                    cancelTransactionBtn.setDisable(false);
                }
            } else {
                card.setCardStatus(CardStatus.Locked);
                DbOperations.updateCard(card);
                labelCardNumber.setText("Card " + card.getCcn() + " is blocked!");
                ccnTextField.clear();
            }

            pinField.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        displayTime();
        pinField.setVisible(false);
        withdrawGridPane.setVisible(false);
        balanceBtn.setDisable(true);
        withdrawalBtn.setDisable(true);
        cancelTransactionBtn.setDisable(true);
        otherAmountBtn.setVisible(false);
        moneyTextField.setVisible(false);

        ccnTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    ccnTextField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

        moneyTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    moneyTextField.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

        newAccountBtn.setOnAction(t -> {
            try {
                Parent root = FXMLLoader.load(getClass().getResource("/fxml/newAccount.fxml"));
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setTitle("Registration Form ");
                Image icon = new Image("/images/icon.png");
                stage.getIcons().add(icon);
                stage.setScene(scene);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch (IOException e) {
                Logger logger = Logger.getLogger(getClass().getName());
                logger.log(Level.SEVERE, "Failed to create new Window.", e);
            }
        });

    }


    public void getMoney(int moneyFromTextField) {
        if (moneyTextField.getText() != null) {
            money = moneyFromTextField;
        } else {
            labelBalance.setText("You need to write an ammount to cash-out!");
        }
    }

    // Card verification method
    public void enterCard(ActionEvent actionEvent) {
        loadCard(ccnTextField.getText());
        if (!card.getCcn().equals(ccnTextField.getText()) || ccnTextField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, ccnTextField.getText() + " you need to enter a valid card!");
            alert.showAndWait();
        } else if (card.getCardStatus() == CardStatus.Locked) {
            Alert locked = new Alert(Alert.AlertType.INFORMATION, ccnTextField.getText() + " was locked due failed pin 3 times!");
            locked.showAndWait();
        } else {
            pinField.setVisible(true);
        }
    }

    // Eject card method
    public void ejectCard(ActionEvent actionEvent) {
        ccnTextField.clear();
        labelBalance.setVisible(false);
        balanceBtn.setDisable(true);
        withdrawalBtn.setDisable(true);
        cancelTransactionBtn.setDisable(true);
        labelCardNumber.setText("Welcome to ATM Simulator App");
        pinField.clear();
        pinField.setVisible(false);
        card = new Card();
        moneyTextField.setVisible(false);
        labelWithdrawAmmount.setText("");
        labelWithdrawAmmount.setVisible(false);
        withdrawGridPane.setVisible(false);
    }


    public void handleButtonAction(ActionEvent event) {
        if (event.getSource() == one) {
            pinField.setText(pinField.getText() + one.getText());
            if (moneyTextField.isVisible()) {
                moneyTextField.setText(moneyTextField.getText() + one.getText());
            }
        } else if (event.getSource() == two) {
            pinField.setText(pinField.getText() + two.getText());
            if (moneyTextField.isVisible()) {
                moneyTextField.setText(moneyTextField.getText() + two.getText());
            }
        } else if (event.getSource() == three) {
            pinField.setText(pinField.getText() + three.getText());
            if (moneyTextField.isVisible()) {
                moneyTextField.setText(moneyTextField.getText() + three.getText());
            }
        } else if (event.getSource() == four) {
            pinField.setText(pinField.getText() + four.getText());
            if (moneyTextField.isVisible()) {
                moneyTextField.setText(moneyTextField.getText() + four.getText());
            }
        } else if (event.getSource() == five) {
            pinField.setText(pinField.getText() + five.getText());
            if (moneyTextField.isVisible()) {
                moneyTextField.setText(moneyTextField.getText() + five.getText());
            }
        } else if (event.getSource() == six) {
            pinField.setText(pinField.getText() + six.getText());
            if (moneyTextField.isVisible()) {
                moneyTextField.setText(moneyTextField.getText() + six.getText());
            }
        } else if (event.getSource() == seven) {
            pinField.setText(pinField.getText() + seven.getText());
            if (moneyTextField.isVisible()) {
                moneyTextField.setText(moneyTextField.getText() + seven.getText());
            }
        } else if (event.getSource() == eight) {
            pinField.setText(pinField.getText() + eight.getText());
            if (moneyTextField.isVisible()) {
                moneyTextField.setText(moneyTextField.getText() + eight.getText());
            }
        } else if (event.getSource() == nine) {
            pinField.setText(pinField.getText() + nine.getText());
            if (moneyTextField.isVisible()) {
                moneyTextField.setText(moneyTextField.getText() + nine.getText());
            }
        } else if (event.getSource() == zero) {
            pinField.setText(pinField.getText() + zero.getText());
            if (moneyTextField.isVisible()) {
                moneyTextField.setText(moneyTextField.getText() + zero.getText());
            }
        } else if (event.getSource() == cancel) {
            pinField.clear();
            ccnTextField.clear();
            moneyTextField.clear();
        } else if (event.getSource() == clear) {
            pinField.clear();
            moneyTextField.clear();
        } else if (event.getSource() == enter) {
            //loadAccount(ccnTextField.getText());
            if (!pinField.getText().isEmpty() && !moneyTextField.isVisible()) {
                validatePin(pinField.getText());
                pinField.clear();
            } else {
                labelCardNumber.setFont(Font.font("Times New Roman", 20));
                labelCardNumber.setText("You need to enter valid pin!. Welcome " + card.getFirstName());
            }
            if (!moneyTextField.getText().isEmpty() && moneyTextField.isVisible() || checkBox100.isSelected()
                    || checkBox200.isSelected() || checkbox400.isSelected() || checkBox500.isSelected()
                    || checkBox1000.isSelected()) {
                if (checkBox100.isSelected() || checkBox200.isSelected() || checkbox400.isSelected() || checkBox500.isSelected() || checkBox1000.isSelected()) {
                    getMoney(Integer.parseInt(checkBoxAmount));
                } else {
                    getMoney(Integer.parseInt(moneyTextField.getText()));
                }

                String initialMoney = String.valueOf(card.getAmount());

                card.setAmount(card.getAmount() - money);
                Transactions trs = new Transactions(null, todaysdate, null, money, null, card);
                List<Transactions> transactionList = Arrays.asList(trs);
                card.setTransactions(transactionList);
                DbOperations.updateCard(card);
                DbOperations.insertNewTransactionByCard(trs);
                infoArea.setText("-----------------------------------" + "\n"
                        + "Account : " + card.getCcn() + "\n"
                        + "First Name : " + card.getFirstName() + "\n"
                        + "Last Name : " + card.getLastName() + "\n"
                        + "Balance : " + initialMoney + "\n"
                        + "Withdrawal amount : " + money + "\n"
                        + "Rest balance : " + card.getAmount() + "\n"
                        + "Date : " + todaysdate + "\n"
                        + "Time : " + todaystime + "\n"
                        + "-----------------------------------");
                // aici trebuie setata tranzactia
                anotherWithrawl();
            }
        }
    }


    // Balance action button
    public void loadBalance(ActionEvent actionEvent) {
        labelBalance.setFont(Font.font("Times New Roman", 18));
        labelBalance.setText("Balance : " + card.getAmount());
        withdrawGridPane.setVisible(false);
        moneyTextField.setVisible(false);
    }

    // Withdraw action button
    public void withdrawAction(ActionEvent actionEvent) {
        if (card.getAmount() < 100) {
            Alert noMoney = new Alert(Alert.AlertType.INFORMATION, "Dear " + card.getFirstName() + " you don't have enought money to withdraw!");
            noMoney.showAndWait();
        } else {
            withdrawGridPane.setVisible(true);
            moneyTextField.setVisible(false);
            pinField.setVisible(false);
            otherAmountBtn.setVisible(true);
        }
    }

    public void anotherAmount() {
        withdrawGridPane.setVisible(false);
        moneyTextField.setVisible(true);
    }

    // CheckBox Validation
    public void checkBox(ActionEvent actionEvent) {
        if (actionEvent.getSource() == checkBox100) {
            checkBox200.setDisable(true);
            checkbox400.setDisable(true);
            checkBox500.setDisable(true);
            checkBox1000.setDisable(true);
            checkBoxAmount = checkBox100.getText();
            moneyTextField.setVisible(false);
            if (!checkBox100.isSelected()) {
                checkBox200.setDisable(false);
                checkbox400.setDisable(false);
                checkBox500.setDisable(false);
                checkBox1000.setDisable(false);
                moneyTextField.setVisible(true);
            }
        } else if (actionEvent.getSource() == checkBox200) {
            checkBox100.setDisable(true);
            checkbox400.setDisable(true);
            checkBox500.setDisable(true);
            checkBox1000.setDisable(true);
            checkBoxAmount = checkBox200.getText();
            moneyTextField.setVisible(false);
            if (!checkBox200.isSelected()) {
                checkBox100.setDisable(false);
                checkbox400.setDisable(false);
                checkBox500.setDisable(false);
                checkBox1000.setDisable(false);
                labelWithdrawAmmount.setText("");
                moneyTextField.setVisible(true);
            }
        } else if (actionEvent.getSource() == checkbox400) {
            checkBox100.setDisable(true);
            checkBox200.setDisable(true);
            checkBox500.setDisable(true);
            checkBox1000.setDisable(true);
            checkBoxAmount = checkbox400.getText();
            moneyTextField.setVisible(false);
            if (!checkbox400.isSelected()) {
                checkBox100.setDisable(false);
                checkBox200.setDisable(false);
                checkBox500.setDisable(false);
                checkBox1000.setDisable(false);
                labelWithdrawAmmount.setText("");
                moneyTextField.setVisible(true);
            }
        } else if (actionEvent.getSource() == checkBox500) {
            checkBox100.setDisable(true);
            checkBox200.setDisable(true);
            checkbox400.setDisable(true);
            checkBox1000.setDisable(true);
            checkBoxAmount = checkBox500.getText();
            moneyTextField.setVisible(false);
            if (!checkBox500.isSelected()) {
                checkBox100.setDisable(false);
                checkBox200.setDisable(false);
                checkbox400.setDisable(false);
                checkBox1000.setDisable(false);
                labelWithdrawAmmount.setText("");
                moneyTextField.setVisible(true);
            }

        } else if (actionEvent.getSource() == checkBox1000) {
            checkBox100.setDisable(true);
            checkBox200.setDisable(true);
            checkbox400.setDisable(true);
            checkBox500.setDisable(true);
            checkBoxAmount = checkBox1000.getText();
            moneyTextField.setVisible(false);
            if (!checkBox1000.isSelected()) {
                checkBox100.setDisable(false);
                checkBox200.setDisable(false);
                checkbox400.setDisable(false);
                checkBox500.setDisable(false);
                labelWithdrawAmmount.setText("");
                moneyTextField.setVisible(true);
            }
        }
    }

    public void anotherWithrawl() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("ATM SIMULATOR");
        alert.setHeaderText(null);
        alert.setContentText("Do you wanna do another withdrawal? \n" + "You still have to withdraw : " + card.getAmount());
        alert.getButtonTypes().clear();
        ButtonType buttonOk = new ButtonType("YES");
        alert.getButtonTypes().add(buttonOk);
        ButtonType buttonNo = new ButtonType("NO");
        alert.getButtonTypes().add(buttonNo);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonOk && card.getAmount() > 100) {
            try {
                infoArea.setText("");
                infoArea.clear();
                moneyTextField.clear();
                moneyTextField.setText("");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            alert.setContentText("You don't have any money to withdrawal!");
            alert.show();
            System.exit(200);
        }
    }
}
