package atmSimulator.controller;

import homeBanking.account.Account;
import homeBanking.dbOperations.AdminDbOperations;
import homeBanking.exception.AdminNotFoundException;
import atmSimulator.card.Card;
import atmSimulator.exceptions.CardNotFoundException;
import atmSimulator.transaction.Transactions;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class AccountController implements Initializable {
    @FXML
    private Label atmLabel;
    @FXML
    private GridPane gridPane;
    @FXML
    private TableColumn<Transactions, String> depositAmountColumn;
    @FXML
    private TableColumn<Transactions, String> depositDateColumn;
    @FXML
    private TableColumn<Transactions, String> withdrawalAmountColumn;
    @FXML
    private TableColumn<Transactions, String> widthdrawalDateColumn;
    @FXML
    private TableView<Transactions> tableView;
    @FXML
    private AnchorPane loginPane;
    @FXML
    private AnchorPane accountPane;
    @FXML
    private Button exitBtn;
    @FXML
    private ListView<Card> listView;
    @FXML
    private Label accountLabel;
    @FXML
    private TextField userField;
    @FXML
    private PasswordField passField;
    @FXML
    private Button loginBtn;
    private Account account;
    private List<Transactions> transactionsByCard;

    public void loadColumns() {
        depositAmountColumn.setCellValueFactory(new PropertyValueFactory<>("depositAmount"));
        depositDateColumn.setCellValueFactory(new PropertyValueFactory<>("depositDate"));
        withdrawalAmountColumn.setCellValueFactory(new PropertyValueFactory<>("withdrawalAmount"));
        widthdrawalDateColumn.setCellValueFactory(new PropertyValueFactory<>("withdrawalDate"));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tableView.setVisible(false);
        accountPane.setVisible(false);
        listView.setOnMouseClicked(event -> {
            String cardTransaction = listView.getSelectionModel().getSelectedItem().getCcn();
            System.out.println(cardTransaction);
            loadColumns();
            loadTransactions(cardTransaction);
            ObservableList<Transactions> observableCardList = FXCollections.observableArrayList(transactionsByCard);
            // observableCardList.add(card);
            tableView.setItems(observableCardList);
        });
        exitBtn.setOnAction(event -> {
            System.exit(0);
        });
    }


    public void loadAccount(String username) {
        try {
            account = AdminDbOperations.getAccount(username);
        } catch (AdminNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void loadTransactions(String cardNumber) {
        try {
            transactionsByCard = AdminDbOperations.getTransactions(cardNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loginAction(ActionEvent actionEvent) {
        if (passField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "You need to enter an invalid password!");
            alert.showAndWait();
        } else {
            loadAccount(userField.getText());
            List<Card> cards = null;
            try {
                cards = AdminDbOperations.getCards(account.getAccountNumber());
            } catch (CardNotFoundException e) {
                e.printStackTrace();
            }
            accountPane.setVisible(true);
            listView.setVisible(true);
            exitBtn.setVisible(true);
            accountLabel.setVisible(true);
            assert cards != null;
            accountLabel.setText("Welcome " + account.getAccountNumber());
            atmLabel.setVisible(false);
            gridPane.setVisible(false);
            listView.getItems().addAll(cards);
            tableView.setVisible(true);
        }
    }
}
