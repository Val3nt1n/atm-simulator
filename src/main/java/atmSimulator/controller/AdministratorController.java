package atmSimulator.controller;

import homeBanking.admin.Admin;
import homeBanking.dbOperations.AdminDbOperations;
import homeBanking.exception.AdminNotFoundException;
import atmSimulator.card.Card;
import atmSimulator.exceptions.CardNotFoundException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class AdministratorController implements Initializable {

    @FXML
    private Button exitBtn;
    @FXML
    private Label userLabel;
    @FXML
    private Button logInBtn;
    @FXML
    private PasswordField passField;
    @FXML
    private TextField userField;
    @FXML
    private AnchorPane logInAnchorPane;
    @FXML
    private AnchorPane cardListViewAnchorPane;
    @FXML
    private ListView<homeBanking.account.Account> listView;
    private Admin admin;
    @FXML
    private TableView<Card> tableView = new TableView<>();
    @FXML
    private TableColumn<Card, String> ccnColumn;
    @FXML
    private TableColumn<Card, String> amountColumn;
    @FXML
    private TableColumn<Card, String> cardStatusColumn;
    @FXML
    private TableColumn<Card, String> fNameColumn;
    @FXML
    private TableColumn<Card, String> lNameColumn;
    @FXML
    private TableColumn<Card, String> pinColumn;


    public void loadColumns() {
        ccnColumn.setCellValueFactory(new PropertyValueFactory<>("ccn"));
        amountColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
        cardStatusColumn.setCellValueFactory(new PropertyValueFactory<>("cardStatus"));
        fNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        pinColumn.setCellValueFactory(new PropertyValueFactory<>("Pin"));
    }


    public void loadAccount(String username) {
        try {
            admin = AdminDbOperations.getAdmin(username);
        } catch (AdminNotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cardListViewAnchorPane.setVisible(false);
        listView.setVisible(false);
        exitBtn.setVisible(false);
        userLabel.setVisible(false);
        exitBtn.setOnAction(t -> System.exit(0));
        tableView.setVisible(false);
        listView.setOnMouseClicked(event -> {
            long cardIndex = listView.getSelectionModel().getSelectedIndex() + 1;
            tableView.setVisible(true);
            System.out.println(cardIndex);
            loadColumns();
            List<Card> card = null;
            try {
                card = AdminDbOperations.getCards(String.valueOf(cardIndex));
            } catch (CardNotFoundException e) {
                e.printStackTrace();
            }
            ObservableList<Card> observableCardList = FXCollections.observableArrayList(card);
            // observableCardList.add(card);
            tableView.setItems(observableCardList);
        });
   }


    public void logIn(ActionEvent actionEvent) {
        loadAccount(userField.getText());
        if (!admin.getPassword().equals(passField.getText()) || passField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "You entered and invalid password!");
            alert.showAndWait();
        } else {
            cardListViewAnchorPane.setVisible(true);
            listView.setVisible(true);
            logInAnchorPane.setVisible(false);
            logInBtn.setVisible(false);
            exitBtn.setVisible(true);
            userLabel.setVisible(true);
            userLabel.setText("Welcome " + admin.getUsername());
            listView.getItems().addAll(AdminDbOperations.getAccounts());
        }
    }

}
