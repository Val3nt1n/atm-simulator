package atmSimulator.db;

import atmSimulator.card.Card;
import atmSimulator.exceptions.CardNotFoundException;
import atmSimulator.transaction.Transactions;
import javafx.scene.control.Alert;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class DbOperations {

    static SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
    static final Logger logger = LoggerFactory.getLogger(DbOperations.class);


    /*
    Method to get one card
    @param : Card
     */
    public static Card getCard(String card) throws CardNotFoundException {
        assert DbOperations.sessionFactory != null;
        try (Session session = DbOperations.sessionFactory.openSession()) {
            Query<Card> queryCard = session.createNamedQuery("getCard", Card.class);
            queryCard.setParameter("cardNumber", card);
            List<Card> cardList = queryCard.list();
            if (cardList.size() == 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, card + " doesen't exist or it's invalid!");
                alert.showAndWait();
                throw new CardNotFoundException("Account " + card + " does not exist!");
            }

            return cardList.get(0);
        } catch (Exception e) {
            System.err.println("Something has happened..." + e);
            throw new CardNotFoundException(e.getMessage());
        }
    }

    /*
    Method to upgrade the card
    @param : Card
     */
    public static void updateCard(Card card) {
        Transaction transaction = null;
        assert DbOperations.sessionFactory != null;
        try {
            Session session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.update(card);
            transaction.commit();
            System.out.println("Updated: " + card);
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }


    /*
    This method is made to insert withdrawal/deposit transactions by card
    @param : Card
    @param : List<Transactions>
     */
    public static void insertNewTransactionByCard(Transactions transactions) {
        Transaction transaction = null;

        try (Session session = sessionFactory.openSession()) {

            transaction = session.beginTransaction();
            session.save(transactions);
            transaction.commit();
            System.out.println(" Transaction : " + transactions);
        } catch (Exception e) {
            logger.error("Failed persisting card's transactions!", e);
            if (transaction != null) transaction.rollback();
        }
    }

}
