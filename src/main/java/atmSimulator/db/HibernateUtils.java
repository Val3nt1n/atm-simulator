package atmSimulator.db;

import homeBanking.account.Account;
import homeBanking.admin.Admin;
import atmSimulator.card.Card;
import atmSimulator.transaction.Transactions;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtils {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        try {
            Configuration configuration = new Configuration();

            Properties properties = new Properties();
            properties.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
            properties.put(Environment.URL, "jdbc:mysql://localhost:3306/atm");
            properties.put(Environment.USER, "root");
            properties.put(Environment.PASS, "Parola123");
            properties.put(Environment.DIALECT, "org.hibernate.dialect.MySQL8Dialect");
            properties.put(Environment.SHOW_SQL, "true");
            properties.put(Environment.FORMAT_SQL, "true");
           //properties.put(Environment.HBM2DDL_AUTO, "create");

            configuration.setProperties(properties);
            configuration.addAnnotatedClass(Admin.class);
            configuration.addAnnotatedClass(Account.class);
            configuration.addAnnotatedClass(Card.class);
            configuration.addAnnotatedClass(Transactions.class);


            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);

            return sessionFactory;
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void cleanUp() {
        if (sessionFactory != null) sessionFactory.close();
    }

}