package atmSimulator.transaction;

import atmSimulator.card.Card;

import javax.persistence.*;

@Entity
@Table(name = "transaction")
@NamedQuery(name = "getTransaction",query = "From Transactions where card_id=:cardNumber")
public class Transactions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    private String withdrawalDate;
    private String depositDate;
    private Integer withdrawalAmount;
    private Integer depositAmount;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "card_id")
    private Card card;


    public Transactions() {
    }

    public Transactions(Long id, String withdrawalDate, String depositDate, Integer withdrawalAmount, Integer depositAmount, Card card) {
        this.id = id;
        this.withdrawalDate = withdrawalDate;
        this.depositDate = depositDate;
        this.withdrawalAmount = withdrawalAmount;
        this.depositAmount = depositAmount;
        this.card = card;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWithdrawalDate() {
        return withdrawalDate;
    }

    public void setWithdrawalDate(String withdrawalDate) {
        this.withdrawalDate = withdrawalDate;
    }

    public String getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(String depositDate) {
        this.depositDate = depositDate;
    }

    public Integer getWithdrawalAmount() {
        return withdrawalAmount;
    }

    public void setWithdrawalAmount(Integer withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

    public Integer getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Integer depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
