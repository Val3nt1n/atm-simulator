package homeBanking.account;

import atmSimulator.card.Card;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "account")
@NamedQuery(name = "getAccount", query = "from Account where accountNumber =:user")
public class Account {
    @Id
    private String accountNumber;
    private String password;
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private List<Card> card = new ArrayList<>();

    public Account() {
    }

    public Account(String accountNumber, String password) {
        this.accountNumber = accountNumber;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public List<Card> getCard() {
        return card;
    }

    public void setCard(List<Card> card) {
        this.card = card;
    }

    @Override
    public String toString() {
        return "Account name : " + accountNumber;
    }
}
