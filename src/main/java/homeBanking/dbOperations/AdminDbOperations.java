package homeBanking.dbOperations;

import homeBanking.account.Account;
import homeBanking.admin.Admin;
import homeBanking.exception.AdminNotFoundException;
import atmSimulator.card.Card;
import atmSimulator.db.HibernateUtils;
import atmSimulator.exceptions.CardNotFoundException;
import atmSimulator.transaction.Transactions;
import javafx.scene.control.Alert;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;

public class AdminDbOperations {

    static SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
    static final Logger logger = LoggerFactory.getLogger(AdminDbOperations.class);

    public static Admin getAdmin(String admin) throws AdminNotFoundException {
        assert AdminDbOperations.sessionFactory != null;
        try (Session session = AdminDbOperations.sessionFactory.openSession()) {
            Query<Admin> queryAdmin = session.createNamedQuery("getUsername", Admin.class);
            queryAdmin.setParameter("user", admin);
            List<Admin> adminList = queryAdmin.list();
            if (adminList.size() == 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, admin + " doesen't exist or it's invalid!");
                alert.showAndWait();
                throw new CardNotFoundException("Account " + admin + " does not exist!");
            }
            return adminList.get(0);
        } catch (Exception e) {
            System.err.println("Something has happened..." + e);
            throw new AdminNotFoundException(e.getMessage());
        }
    }

    public static Account getAccount(String account) throws AdminNotFoundException {
        assert AdminDbOperations.sessionFactory != null;
        try (Session session = AdminDbOperations.sessionFactory.openSession()) {
            Query<Account> queryAdmin = session.createNamedQuery("getAccount", Account.class);
            queryAdmin.setParameter("user", account);
            List<Account> adminList = queryAdmin.list();
            if (adminList.size() == 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, account + " doesen't exist or it's invalid!");
                alert.showAndWait();
                throw new CardNotFoundException("Account " + account + " does not exist!");
            }
            return adminList.get(0);
        } catch (Exception e) {
            System.err.println("Something has happened..." + e);
            throw new AdminNotFoundException(e.getMessage());
        }
    }

    public static List<Account> getAccounts() {
        assert sessionFactory != null;
        try (Session session = sessionFactory.openSession()) {
            Query<Account> result = session.createQuery("from Account", Account.class);
            return result.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    public static List<Card> getCards(String cardIndex) throws CardNotFoundException {
        assert AdminDbOperations.sessionFactory != null;
        try (Session session = AdminDbOperations.sessionFactory.openSession()) {
            Query<Card> queryAdmin = session.createNamedQuery("getAccountFromCard", Card.class);
            queryAdmin.setParameter("accountIndex", cardIndex);
            List<Card> adminList = queryAdmin.list();
            if (adminList.size() == 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, cardIndex + " doesen't exist or it's invalid!");
                alert.showAndWait();
                throw new CardNotFoundException("Account " + cardIndex + " does not exist!");
            }
            return adminList;
        } catch (Exception e) {
            System.err.println("Something has happened..." + e);
            throw new CardNotFoundException(e.getMessage());
        }
    }

    public static List<Transactions> getTransactions(String cardIndex) throws CardNotFoundException {
        assert AdminDbOperations.sessionFactory != null;
        try (Session session = AdminDbOperations.sessionFactory.openSession()) {
            Query<Transactions> queryAdmin = session.createNamedQuery("getTransaction", Transactions.class);
            queryAdmin.setParameter("cardNumber", cardIndex);
            List<Transactions> adminList = queryAdmin.list();
            if (adminList.size() == 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, cardIndex + " doesen't exist or it's invalid!");
                alert.showAndWait();
                throw new CardNotFoundException("Account " + cardIndex + " does not exist!");
            }
            return adminList;
        } catch (Exception e) {
            System.err.println("Something has happened..." + e);
            throw new CardNotFoundException(e.getMessage());
        }
    }
/*
    public static Card getCard(String id) {
        assert sessionFactory != null;
        try (Session session = sessionFactory.openSession()) {
            Query<Card> result = session.createQuery("from Card where account_id=:id", Card.class);
            return result.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            //return Collections.emptyList();
        }
    }

 */

    public static Card getCard(String card) throws AdminNotFoundException {
        assert AdminDbOperations.sessionFactory != null;
        try (Session session = AdminDbOperations.sessionFactory.openSession()) {
            Query<Card> result = session.createQuery("from Card where account_id=:id", Card.class);
            result.setParameter("user", card);
            List<Card> adminList = result.list();
            if (adminList.size() == 0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION, card + " doesen't exist or it's invalid!");
                alert.showAndWait();
                throw new CardNotFoundException("Account " + card + " does not exist!");
            }
            return adminList.get(0);
        } catch (Exception e) {
            System.err.println("Something has happened..." + e);
            throw new AdminNotFoundException(e.getMessage());
        }
    }

}
