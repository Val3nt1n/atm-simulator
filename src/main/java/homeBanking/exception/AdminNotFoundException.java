package homeBanking.exception;


public class AdminNotFoundException extends Exception {


    private String message;

    public AdminNotFoundException(String string) {
        this.message = string;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public AdminNotFoundException() {
    }
}


